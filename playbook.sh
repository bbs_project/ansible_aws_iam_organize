#!/bin/bash

# Check if "all" is provided as the first argument
if [ "$1" = "all" ]; then
    for file in environments/local/*; do
        if [ -f "$file" ]; then
            ansible-playbook -i environments/local/"$(basename "$file")" env_config.yml --tags 'iam_role'
        fi
    done

else
    ansible-playbook -i environments/local/"$1" env_config.yml --tags 'iam_role'
fi
