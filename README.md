## How to add

Add host files in environment/local
Ex: sepbase

Add env file with the same name group_vars
Ex: sepbase.yml

## How to run

To orgnize a account with config role:

```
ansible-playbook -i environments/local env_config.yml  --tags 'iam_role' --limit 'septeni-vietnam'

To apply on all account

./trigger_playbook.sh all

To apply on a specific account
./trigger_playbook.sh sepbase
```
